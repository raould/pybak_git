*** warning: there be dragons in here. ***

extra dependencies:
- sever vs. client have different python lib dependencies
- python image library
- python-magic
- whatever else is missing when you try to run it e.g. pathlib2, requests...

notes:
- gpl v3 license, please.
- lets you back up files to a central place.
- uses md5 hash to deduplicate.
- builds html files to let you browse the backups.
- browse shows all originating places of files.

examples:
start on server 192.168.123.42
sudo ./pybadkd.py &
will run on default port of 6969.
run client to push files:
client ~/Stufftobackup 192.168.123.42
will push over to default port of 6969.

generating browsable pages:
[this is based on how i have dirs set up locally.]
- buildhtml no longer crawls the canonical dirs.
- the server logs to ~/tmp_running/newest-* files as it gets backups from clients.
- buildhtml will read ~/tmp_running/newest-* as well as ~/tmp/newest.
- for all the "old" files, you need to re-generate ~/tmp/newest by running build_times.sh, which does crawl all of canonical.
- buildhtml-pop"ular".sh to restrict the regeneration to --images-only, or --not-images since most often we're only backing up photos these days.

backups:
- the server uploads smaller images as further backups to s3 as clients send backups.
- you have to have s3 credentials set up ie in ~/.aws/credentials.
- rclone.sh uses rclone to do encrypted backup to B2, using ie ~/.config/rclone/rclone.conf.

