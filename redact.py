#!/usr/bin/env python

import util
import sys
import os
import os.path
import shutil
import glob
import re
import urllib
import distutils
from distutils import dir_util

########################################

redacted_dir = "/home/pybak/Redacted"
if not os.path.exists(redacted_dir):
    print("missing redacted_dir %s" % redacted_dir)
    exit(1)

if len(sys.argv) < 2:
    print("source file(s) or url(s) to redact?")
    sys.exit(1)

########################################

def move(src):
    src_parent = util.get_parent_path(src)
    print("src_parent = %s" % src_parent)
    if not os.path.isdir(src_parent):
        print("src_parent is not a directory? %s" % src_parent)
    dst_parent = os.path.join(redacted_dir, os.path.split(src.lstrip(os.path.sep))[0])
    print("dst_parent = %s" % dst_parent)
    distutils.dir_util.mkpath(dst_parent)
    src_filename = os.path.basename(src)
    dst_path = os.path.join(dst_parent, src_filename)
    if os.path.exists(src):
        shutil.copy2(src, dst_path)
        if not os.path.exists(dst_path):
            print("failed to copy to dst %s" % dst_path)
    print("dst_path = %s" % dst_path)

########################################

def redact_one(src0):

    file = re.sub(".*browse/", "/home/pybak/browse/", src0)
    file = urllib.unquote(file)
    file = os.path.abspath(file)
    print("src = %s" % file)

    if not os.path.lexists(file):
        print("source does not exist %s" % file)
        try:
            print(os.stat(file))
        except:
            pass
        os.system("ls -l %s" % re.sub(" ", "\ ", file))
        os.system("file %s" % re.sub(" ", "\ ", file))
        sys.exit(1)

    if os.path.isdir(file):
        print("source is not a file %s" % file)
        print(os.stat(file))
        os.system("ls %s" % file)
        sys.exit(1)

    ########################################

    dstsymlink = os.path.abspath(os.path.realpath(file))
    print("dstsymlink = %s" % dstsymlink)

    if file != dstsymlink:
        print("removing dstsymlink")
        move(dstsymlink)
        if os.path.lexists(dstsymlink):
            os.remove(dstsymlink)
        for f in glob.glob("%s*" % dstsymlink):
            if os.path.lexists(f):
                os.remove(f)

    print("removing src file")
    if os.path.lexists(file):
        os.remove(file)

########################################

for src in sys.argv[1:]:
    redact_one(src)
