#!/usr/bin/env bash

TMP=/home/pybak/tmp

rm -f ${TMP}/newest*

sort -rm ${TMP}/*.stats | split -dl 1000 - ${TMP}/newest
