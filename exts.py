# a grab bag of file extensions taken from the internet somewhere.
# video and raw formats are (re)specified way below, after this first list.
exts = [
"3dm",
"3g2",
"3gp",
"7z",
"8bi",
"accdb",
"ai",
"aif",
"app",
"asf",
"asp",
"aspx",
"asx",
"avi",
"bak",
"bat",
"bin",
"bmp",
"bz2",
"c",
"cab",
"cer",
"cfg",
"cfm",
"cgi",
"class",
"com",
"cpl",
"cpp",
"cs",
"csr",
"css",
"csv",
"cue",
"cur",
"dat",
"db",
"dbf",
"dbx",
"dds",
"deb",
"dem",
"dll",
"dmg",
"dmp",
"doc",
"docx",
"drv",
"dtd",
"dwg",
"dxf",
"efx",
"eps",
"epub",
"exe",
"fla",
"flv",
"fnt",
"fon",
"font",
"gadget",
"gam",
"gbr",
"ged",
"gif",
"gpx",
"gz",
"gzip",
"hqx",
"htm",
"html",
"ibooks",
"icns",
"ico",
"icon",
"ics",
"iff",
"indd",
"ini",
"iso",
"jar",
"java",
"jpeg",
"jpg",
"js",
"jsp",
"key",
"keychain",
"kml",
"lnk",
"log",
"m",
"m3u",
"m4a",
"max",
"mdb",
"mid",
"mim",
"mime",
"mov",
"mts",
"mp3",
"mp4",
"mpa",
"mpeg",
"mpg",
"msg",
"msi",
"nef",
"nes",
"obj",
"odt",
"otf",
"pages",
"part",
"pct",
"pdb",
"pdf",
"php",
"pif",
"pkg",
"pl",
"plugin",
"png",
"pps",
"ppt",
"pptx",
"prf",
"ps",
"psd",
"pspimage",
"py",
"ra",
"rar",
"rm",
"rom",
"rpm",
"rss",
"rtf",
"sav",
"sdf",
"sit",
"sitx",
"sql",
"srt",
"svg",
"swf",
"sys",
"tar",
"tar.gz",
"tar.Z",
"tax2010",
"tex",
"tga",
"thm",
"tif",
"tiff",
"tmp",
"toast",
"torrent",
"ttf",
"txt",
"uue",
"vb",
"vcd",
"vcf",
"vob",
"wav",
"wma",
"wmv",
"wpd",
"wps",
"wsf",
"xhtm",
"xhtml",
"xll",
"xlr",
"xls",
"xlsx",
"xml",
"yuv",
"zip",
"zipx",
]
exts_set = set(exts)

# https://en.wikipedia.org/wiki/Video_file_format
videoExts = [
    "webm",
    "mkv",
    "flv",
    "vob",
    "ogv", "ogg",
    "drc",
    "gif",
    "gifv",
    "mng",
    "avi",
    "mts", "m2ts", "ts",
    "mov", "qt",
    "wmv",
    "yuv",
    "rm",
    "rmvb",
    "viv",
    "asf",
    "amv",
    "mp4", "m4p", "m4v",
    "mpg", "mp2", "mpeg", "mpe", "mpv",
    "m2v",
    "m4v",
    "svi",
    "3gp",
    "3g2",
    "mxf",
    "roq",
    "nsv",
    "flv", "f4v", "f4p", "f4a", "f4b"
]
videoExts_set = set(videoExts)

# https://en.wikipedia.org/wiki/Raw_image_format#Raw_filename_extensions_and_respective_camera_manufacturers
rawExts = [
"raw",
"nef",
"nrw",
"crw",
"cr2",
"dcs",
"dcr",
"drf",
"dng",
"erf",
"orf",
"pef",
"ptx",
"rw2",
"rwl",
]
rawExts_set = set(rawExts)

all_set = set(exts)
all_set.update(rawExts)
all_set.update(videoExts)

all = list(all_set)
