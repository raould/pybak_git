#!/usr/bin/env python

import sys
import os
import os.path
import shutil
import re
import util
import visit_core
import urllib

# extract / export a copy of the whole
# pybak canonical & browse db, of only
# the things in browse/images_by_date.
# !!! this is all very painfully hard
# coded to the current db format. !!!

DEBUG = False

assert len(sys.argv) >= 2, "tell me where to extract: <dir>"
DST_PATH = sys.argv[1]
assert DST_PATH != None and DST_PATH != "", "tell me where to extract: <dir>"
assert os.path.isdir( DST_PATH ), "tell me a directory for extract"
CANONICAL = "/home/pybak/canonical"
BROWSE = "/home/pybak/browse"
IMAGES = "/home/pybak/browse/image_by_date/000_shallow"
HTTPS = "https://www.psync-o-pathics.com"
HTTPS_BROWSE = "%s/browse" % HTTPS
HTTPS_CANONICAL = "%s/canonical" % HTTPS
DST_BROWSE = os.path.join( DST_PATH, BROWSE.lstrip(os.path.sep) )
DST_CANONICAL = os.path.join( DST_PATH, CANONICAL.lstrip(os.path.sep) )
assert DST_PATH not in CANONICAL, "dangerous overlap"
assert DST_PATH not in BROWSE, "dangerous overlap"
assert DST_PATH not in IMAGES, "dangerous overlap"
print "extracting to", DST_BROWSE, DST_CANONICAL

LOCALIZED_SHEET = "/tmp/_pybak_localized_sheet"
LOCALIZED_HTML = "/tmp/_pybak_localized_html"

# you would not believe how much i hate python.
def log( *args ):
    if DEBUG:
        try:
            print " ".join(args)
        except:
            print args
def logf( *args ):
    print args

def to_extract_path( src ):
    return os.path.join( DST_PATH, src.lstrip(os.path.sep) )

def check_extract_exists( src ):
    dst = to_extract_path( src )
    return os.path.exists( dst )

def extract_file( src ):
    dst = to_extract_path( src )
    util.ensure_parent_path( dst )
    shutil.copyfile( src, dst )
    log( " ", src, "->", dst )

def browse_https2src( browse_file ):
    assert "browse" in browse_file, browse_file
    assert browse_file.startswith( "https" ), browse_file
    dst = re.sub( '^%s' % HTTPS_BROWSE, BROWSE, browse_file )
    return urllib.unquote( dst )

def canonical_https2src( canonical_file ):
    assert "canonical" in canonical_file, canonical_file
    assert canonical_file.startswith( "https" ), canonical_file
    dst = re.sub( '^%s' % HTTPS_CANONICAL, CANONICAL, canonical_file )
    return urllib.unquote( dst )

def browse_src2dst( browse_file, from_file ):
    assert "browse" in browse_file, browse_file
    if browse_file.startswith( "https" ):
        dst = re.sub( '^%s' % HTTPS_BROWSE, DST_BROWSE, browse_file )
        dst = urllib.unquote( dst )
    elif browse_file.startswith( BROWSE ):
        dst = re.sub( BROWSE, DST_BROWSE, browse_file )
    else:
        assert False, browse_file
    dst_rel = os.path.relpath( dst, util.get_parent_path(from_file) )
    return dst_rel

def canonical_src2dst( canonical_file, from_file ):
    assert "canonical" in canonical_file, canonical_file
    if canonical_file.startswith( "https" ):
        dst = re.sub( '^%s' % HTTPS_CANONICAL, DST_CANONICAL, canonical_file )
        dst = urllib.unquote( dst )
    elif canonical_file.startswith( CANONICAL ):
        dst = re.sub( CANONICAL, DST_CANONICAL, canonical_file )
    else:
        assert False, canonical_file
    dst_rel = os.path.relpath( dst, util.get_parent_path(from_file) )
    return dst_rel

def extract_html_file( html_src ):
    log( "extract_html_file", html_src )
    html_dst = to_extract_path( html_src )
    with open( LOCALIZED_HTML, "w" ) as w:
        with open( html_src, "r" ) as r:
            for line in r:
                bs = re.findall( '%s[^"> ]*' % HTTPS_BROWSE, line )
                for b in bs:
                    line = re.sub( b, browse_src2dst( b, html_dst ), line )
                cs = re.findall( '%s[^"> ]*' % HTTPS_CANONICAL, line )
                for c in cs:
                    line = re.sub( c, canonical_src2dst( c, html_dst ), line )
                w.write( line )
    util.ensure_parent_path( html_dst )
    shutil.copyfile( LOCALIZED_HTML, html_dst )
    log( " %s -> %s -> %s" % ( html_src, LOCALIZED_HTML, html_dst ) )

def extract_syms_dirs( syms_dirs ):
    for syms_dir in syms_dirs:
        log( " extract_syms_dirs", syms_dir )
        dirlist = os.listdir( syms_dir )
        for i in range(len(dirlist)):
            s = dirlist[i]
            alias_src = os.path.join( syms_dir, s )
            assert os.path.islink( alias_src ), alias_src
            target_src = os.readlink( alias_src )
            if os.path.exists( target_src ):
                extract_file( target_src )

                alias_dst = to_extract_path( alias_src )
                util.remove( alias_dst )
                util.ensure_parent_path( alias_dst )

                target_dst = to_extract_path( target_src )
                assert os.path.exists( target_dst ), "%s %s" % (target, target_dst)

                target_dst_rel = os.path.relpath( target_dst, util.get_parent_path(alias_dst) )

                log( " symlink (%s/%s)" % (i+1, len(dirlist)), target_dst_rel, alias_dst )
                os.symlink( target_dst_rel, alias_dst )

def extract_this_sheet( sheet_src ):
    log( " extract_this_sheet", sheet_src )
    # fix the http links to be relative filesystem paths.
    sheet_dst = to_extract_path( sheet_src )
    with open( LOCALIZED_SHEET, "w" ) as w:
        with open( sheet_src, "r" ) as r:
            for line in r:
                found = re.findall( '%s[^"> ]*' % HTTPS_BROWSE, line )
                for src in found:
                    line = re.sub( src, browse_src2dst( src, sheet_dst ), line )
                w.write( line )
    util.ensure_parent_path( sheet_dst )
    shutil.copyfile( LOCALIZED_SHEET, sheet_dst )
    log( " %s -> %s -> %s" % ( sheet_src, LOCALIZED_SHEET, sheet_dst ) )

def extract_sheet_files( sheet ):
    log( " extract_sheet_files", sheet )
    data = {}
    syms_dirs = set([])
    with open( sheet ) as f:
        for line in f:
            if "a href" in line:
                found = re.findall( '%s[^"> ]*' % HTTPS_BROWSE, line )
                assert len(found) == 2, found
                html_src = browse_https2src( found[0] )
                thumb_src = browse_https2src( found[1] )
                # this is probably the same for many images so do it once at the end.
                syms_dir = os.path.join( util.get_parent_path( html_src ), ".syms" )
                syms_dirs.add( syms_dir )
                # woweth this sucketh, hacky implict converting from browse to canonical.
                canonical_src = re.sub(
                    "_thumb.jpg",
                    "",
                    re.sub(
                        ".genimgs/",
                        "",
                        re.sub( BROWSE, CANONICAL, thumb_src )
                    )
                )
                data[canonical_src] = {
                    "thumb": thumb_src,
                    "html": html_src,
                }
        for c in data:
            log( " extracting", c )
            extract_file( c )
            extract_file( data[c]["thumb"] )
            extract_html_file( data[c]["html"] )
    extract_syms_dirs( syms_dirs )

def extract_dir( d ):
    sheet = os.path.join( d, "000_thumbnails.html" )
    extract_sheet_files( sheet )
    extract_this_sheet( sheet )

dirlist = os.listdir( IMAGES )
for i in range(len(dirlist)):
    e = dirlist[i]
    full_path = os.path.join( IMAGES, e )
    if os.path.isdir( full_path ) or os.path.islink( full_path ):
        logf( " extracting dir (%s/%s)" % (i+1, len(dirlist)), full_path )
        extract_dir( full_path )

