#!/usr/bin/env bash

CANONICAL=/home/pybak/canonical
TMP=/home/pybak/tmp

rm -rf ${TMP}/*

for outer in `seq 0 255 | while read n; do printf "%02x " $n; done`; do
    for inner in `seq 0 255 | while read n; do printf "%02x " $n; done`; do
	dir=${CANONICAL}/${outer}/${inner}
	output=${TMP}/${outer}${inner}.stats
	echo ${dir}
	if [ -e "${dir}" ]; then
	    find \
		${dir} \
	        -mindepth 14 \
		-not -iname \*.mdj \
		-and -not -iname \*.mdj.\* \
		-and -not -iname \*_web.png \
		-and -not -iname \*.metadata \
		-type f \
		-exec stat -c "%Y %n" {}.mdj \; | \
		sed 's/.mdj$//' | \
		sort -n -r >> ${output}
	    echo ${dir} `wc -l ${output}`
	fi
	done
done

# there were too many files to sort with a single command, thus this 2-phase sorting.
for outer in `seq 0 255 | while read n; do printf "%02x " $n; done`; do
    echo "sorting ${outer}"
    sort -m -r ${TMP}/${outer}*.stats > ${TMP}/${outer}.tmp
done
sort -m -r ${TMP}/*.tmp > ${TMP}/newest
rm -f ${TMP}/*.tmp
