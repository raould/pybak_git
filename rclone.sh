#!/usr/bin/env bash

# want to have the same name of the local src path
# used for the remote dst path, but then we have
# to deal with "/" in between and we have to also
# worry about absolute vs. absolute-canonical (realpath)
# differences. i want absolute-non-canonical i feel.

DST_BUCKET="b2-pybak-crypt"

declare -A srcrel2dst
srcrel2dst["/home/pybak/canonical/"]="v2/canonical"
srcrel2dst["/mnt/nas/nfsshare/canonical_old"]="v2/canonical_old"

for srcrel in "${!srcrel2dst[@]}"; do
    dst=${srcrel2dst[${srcrel}]}
    src=$(realpath ${srcrel}) 
    echo "scanning from root = ${src}..."
    echo "dst: ${dst}"
    found=`find ${src} -mindepth 1 -maxdepth 1 -type d | sort -n`
    echo "found: ${found}"
    for found in ${found}; do
	rcmd="sync ${found} ${DST_BUCKET}:${dst}${found}"
	echo "+ found: ${found}"
	echo " rcmd: ${rcmd}"
	nice rclone --tpslimit 14 -P -L ${rcmd}
    done
done
